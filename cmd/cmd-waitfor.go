package cmd

import (
	"github.com/spf13/cobra"

	"gitlab.com/ytopia/ops/dep/waitfor"
)

func CmdWaitfor(app App) *cobra.Command {

	var cmd = &cobra.Command{
		Use:   "waitfor [flags] my-service:8080 [--] [command]",
		Short: "Dependencies manager for container",
		Long:  "Wait for a list of dependencies: ping (host), a tcp (host:port), http (url code), script or scripts-dir to run a program or exit",
		Args:  cobra.MinimumNArgs(1),
		PreRun: func(cmd *cobra.Command, args []string) {
			app.OnPreRun(cmd)
		},
		Example: `
# ping:
  dep waitfor host
  #is equivalent to:
    dep waitfor '{type:"ping",target:"host"}'

# tcp:
  dep waitfor hostname:80
  #is equivalent to:
    dep waitfor '{type:"tcp",target:"hostname:80"}'

# http:
  dep waitfor https://gitlab.com
  # is equivalent to:
    dep waitfor '{type:"http",target:"https://gitlab.com"}'
    # with ports range:
    dep waitfor '{type:"http",target:["https://gitlab.com","200-299"]}'

  # with expected response codes
  dep waitfor '[["https://gitlab.com",200,301]]'
  # is equivalent to:
    dep waitfor '[{type:"http",target:["https://gitlab.com",200,301]}]'

  # with unexpected response codes
  dep waitfor '[["https://gitlab.com","!500"]]'
  # is equivalent to:
    dep waitfor '[{type:"http",target:["https://gitlab.com","!500"]}]'

# script:
  dep waitfor /absolute/path/to/my/script
  # is equivalent to:
    dep waitfor '{type:"script",target:"/absolute/path/to/my/script"}'

  # with arguments:
  dep waitfor '{type:"script",target:["/absolute/path/to/my/script","arg1","arg2"]}'
  #with relative path:
  dep waitfor '{type:"script",target:["relative/path/to/my/script","arg1","arg2"]}'

# scripts-dir:
  dep waitfor /path/to/my/scripts-dir/
  # is equivalent to:
    dep waitfor waitfor '{type:"scripts-dir",target:"/path/to/my/scripts-dir"}'

# combine them:
waitfor '["host","https://giltab.com",{type:"script",target:["my-script","arg"]}]'

`,
		Run: func(cobraCmd *cobra.Command, argv []string) {
			waitfor.Cmd(argv, app)
		},
	}

	flags := cmd.Flags()

	flags.StringP("retry", "r", "false", "default retry for dependencies as number or true for no limit")
	flags.StringP("timeout", "t", "3s", "default retry timeout for dependencies")
	flags.StringP("delay", "d", "2s", "default retry delay for dependencies")

	return cmd

}
