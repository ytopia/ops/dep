package tools

import (
	"os"

	"github.com/sirupsen/logrus"
)

func EnvExpandMap(args ...interface{}) map[string]string {
	envmap := make(map[string]string)

	envmapper := func(key string) string {
		return envmap[key]
	}

	for _, arg := range args {
		var envmerge map[string]string
		switch arg.(type) {
		case []string:
			envmerge = EnvToMap(arg.([]string))
		case map[string]string:
			envmerge = arg.(map[string]string)
		default:
			logrus.Fatalf(`invalid argument for EnvExpandMap, expected []string or map[string]string, received type:"%T",value:"%v"`, arg, arg)
		}
		for k, v := range envmerge {
			if k != "" {
				envmap[k] = os.Expand(v, envmapper)
			}
		}
	}

	return envmap
}
