package tools

import (
	"regexp"
)

func ValidateHostname(host string) bool {
	validHostnameRegex, _ := regexp.Compile("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]).)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9-]*[A-Za-z0-9])$")
	return validHostnameRegex.MatchString(host)
}
