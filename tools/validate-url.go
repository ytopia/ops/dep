package tools

import (
	"net/url"
)

func ValidateUrl(str string) bool {
	if !((len(str) >= 7 && str[0:7] == "http://") || (len(str) >= 8 && str[0:8] == "https://")) {
		return false
	}
	if _, err := url.ParseRequestURI(str); err == nil {
		return true
	}
	return false
}
