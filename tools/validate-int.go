package tools

import (
	"strconv"
)

func ValidateInt(str string) bool {
	if _, err := strconv.Atoi(str); err == nil {
		return true
	}
	return false
}
