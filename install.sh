#!/usr/bin/env bash
set -e

DEP_VERSION=${DEP_VERSION:-latest}
DEP_GITLAB_PROJECT_ID=`curl -s https://gitlab.com/api/v4/projects/ytopia%2Fops%2Fdep/ | jq .id`
[ "$DEP_VERSION" = "latest" ] && DEP_VERSION=`curl -s "https://gitlab.com/api/v4/projects/${DEP_GITLAB_PROJECT_ID}/repository/tags?search=^v&order_by=updated&sort=desc" | jq -r '.[0] | .name'`
DEP_ARTIFACT_URL=`curl -s https://gitlab.com/api/v4/projects/$DEP_GITLAB_PROJECT_ID/releases/${DEP_VERSION}/assets/links | jq -r '.[] | select(.name | contains("dep_'${DEP_VERSION}'_linux_amd64")) | .url'`
curl -o dep -s $DEP_ARTIFACT_URL
chmod +x dep
mv dep /usr/local/bin/
which dep

echo installed