package waitfor

import (
	"sync"
	"time"
	"context"

	"github.com/sirupsen/logrus"
	"github.com/yosuke-furukawa/json5/encoding/json5"
)

type DependencyGroup struct {
	App          App
	Env          []string
	Dependencies []*Dependency
	Environ      []string
}

func Dependencies(dependenciesJSON string, app App, environ []string, ctx *context.Context) (bool, error) {

	if ctx == nil {
		defaultCtx := context.Background()
		ctx = &defaultCtx
	}

	dependencyGroup := NewDependencyGroup(dependenciesJSON, app, environ)

	var ok bool = true
	var err error

	logrus.Infof("dependencies: %v", dependenciesJSON)

	dependenciesLength := len(dependencyGroup.Dependencies)
	if dependenciesLength > 0 {

		var wg sync.WaitGroup
		wg.Add(dependenciesLength)

		quit := make(chan struct{})
		var quitOnce sync.Once
		quitOnceFunc := func() {
			close(quit)
		}

		startedTime := time.Now()
		for _, dependency := range dependencyGroup.Dependencies {
			go func(dependency *Dependency) {
				defer wg.Done()
				resolved, failed := dependency.Wait(startedTime, quit, ctx)
				if !resolved {
					ok = false
					if failed != nil {
						err = failed
					}
					quitOnce.Do(quitOnceFunc)
				}
			}(dependency)
		}
		wg.Wait()
	}

	if ok {
		logrus.Info("dependencies ready")
	} else if err != nil {
		logrus.Error("dependencies failed")
	}

	return ok, err
}

func NewDependencyGroup(dependenciesJSON string, app App, environ []string) *DependencyGroup {
	dependencyGroup := &DependencyGroup{}
	dependencyGroup.App = app
	dependencyGroup.Environ = environ

	var dependenciesValues []interface{}

	if dependenciesJSON[0:1] == "[" {
		err := json5.Unmarshal([]byte(dependenciesJSON), &dependenciesValues)
		if err != nil {
			logrus.Fatal(err)
		}
	} else if dependenciesJSON[0:1] == "{" {
		var dependenciesValue interface{}
		err := json5.Unmarshal([]byte(dependenciesJSON), &dependenciesValue)
		if err != nil {
			logrus.Fatal(err)
		}
		dependenciesValues = []interface{}{dependenciesValue}
	} else {
		dependenciesValues = []interface{}{dependenciesJSON}
	}

	dependencyGroup.Dependencies = make([]*Dependency, len(dependenciesValues))
	for i, dep := range dependenciesValues {
		dependencyGroup.Dependencies[i] = NewDependency(dep, dependencyGroup)
	}

	return dependencyGroup
}
