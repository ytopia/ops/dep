package waitfor

import (
	"net"
	"strings"
	"time"
)

func CheckTcp(host string, port string, timeout time.Duration) bool {

	d := net.Dialer{Timeout: timeout}
	_, err := d.Dial("tcp", strings.Join([]string{host, port}, ":"))

	if err == nil {
		return true
	} else {
		return false
	}
}
