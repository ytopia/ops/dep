package waitfor

import (
	"context"
	"os"
	"os/exec"

	"github.com/sirupsen/logrus"
	ping "github.com/sparrc/go-ping"
	"golang.org/x/net/icmp"
)

func CheckPing(host string, ctx context.Context) bool {

	_, err := icmp.ListenPacket("ip4:icmp", "")
	if err != nil {
		logrus.Debug("unable to use icmp to use native go ping, call: `setcap cap_net_raw=+ep " + os.Args[0] + "`")
		logrus.Debug("fallback to use native system ping")
		commandPath, lookErr := exec.LookPath("ping")
		if lookErr != nil {
			logrus.Errorf("unable to use native go ping and to locate native system ping")
			return false
		}
		argsSlice := []string{"-c", "1", host}
		cmd := exec.CommandContext(ctx, commandPath, argsSlice...)
		err := cmd.Run()
		return err == nil
	}

	pinger, err := ping.NewPinger(host)
	if err != nil {
		return false
	}

	go func() {
		select {
		case <-ctx.Done():
			pinger.Stop()
		}
	}()

	pinger.SetPrivileged(true)
	pinger.Count = 1
	pinger.Run() // blocks until finished
	stats := pinger.Statistics()
	if stats.PacketsRecv >= 1 {
		return true
	}
	return false
}
