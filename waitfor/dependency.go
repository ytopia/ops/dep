package waitfor

import (
	"context"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/ytopia/ops/dep/decode"
	"gitlab.com/ytopia/ops/dep/tools"
)

type Dependency struct {
	Ctx       context.Context
	CtxCancel context.CancelFunc

	DependencyGroup *DependencyGroup
	Type            string
	Target          []string
	Retry           interface{}
	Timeout         *time.Duration
	Delay           *time.Duration
	CleanEnv        *bool
	Env             map[string]string

	Environ    []string
	EnvironMap map[string]string
}

func (dependency *Dependency) Wait(startedTime time.Time, quit chan struct{}, globalCtx *context.Context) (bool, error) {

	var timeoutTime time.Time
	ctx := context.Background()
	var cancel context.CancelFunc
	if dependency.Timeout != nil {
		timeoutTime = startedTime.Add(*dependency.Timeout)
		ctx, cancel = context.WithTimeout(ctx, *dependency.Timeout)
	} else {
		ctx, cancel = context.WithCancel(ctx)
	}
	dependency.Ctx = ctx
	dependency.CtxCancel = cancel

	go func() {
		for {
			select {
			case <-(*globalCtx).Done():
				dependency.CtxCancel()
				return
			}
		}
	}()

	retry := dependency.Retry
	var retryTypeInt bool = false
	var retryTypeBool bool = false
	switch retry.(type) {
	case int:
		retryTypeInt = true
	case bool:
		retryTypeBool = true
	}

	try := 0
	for {
		select {
		case <-quit:
			return false, nil
		case <-ctx.Done():
			return false, nil
		default:
			if try == 1 {
				logrus.Infof("%s %v: waiting...", dependency.Type, dependency.Target)
			}
			if try > 0 {
				logrus.Debugf("%s %v: delay %v", dependency.Type, dependency.Target, dependency.Delay)
				time.Sleep(*dependency.Delay)
			}
			logrus.Debugf("%s %v: resolving try: %v...", dependency.Type, dependency.Target, try+1)
			var resolved bool
			switch dependency.Type {
			case "ping":
				host := dependency.Target[0]
				resolved = CheckPing(host, ctx)
			case "tcp":
				var netParts []string
				if len(dependency.Target) == 1 {
					netParts = strings.Split(dependency.Target[0], ":")
					if len(netParts) == 3 {
						netParts = netParts[1:3]
					}
				} else {
					netParts = dependency.Target[0:2]
				}
				host := netParts[0]
				port := netParts[1]
				resolved = CheckTcp(host, port, *dependency.Timeout)
			case "script":
				resolved = CheckScript(dependency.Target, dependency.Environ, ctx)
			case "scripts-dir":
				resolved = CheckScriptsDir(dependency.Target[0], dependency.Environ, ctx)
			case "http":
				resolved = CheckHttp(dependency.Target, ctx)
			}

			if resolved {
				logrus.Infof("%s %v: ready", dependency.Type, dependency.Target)
				return true, nil
			} else if (retryTypeInt && try >= retry.(int)) || (retryTypeBool && !retry.(bool)) {
				logrus.Errorf("%s %v: failed retry=%v", dependency.Type, dependency.Target, retry)
				return false, fmt.Errorf("%s %v: failed retry=%v", dependency.Type, dependency.Target, retry)
			} else if dependency.Timeout != nil && time.Now().After(timeoutTime) {
				logrus.Errorf("%s %v: failed retry-timeout=%v", dependency.Type, dependency.Target, dependency.Timeout)
				return false, fmt.Errorf("%s %v: failed retry-timeout=%v", dependency.Type, dependency.Target, dependency.Timeout)
			} else {
				try++
			}
		}
	}

}

func (dependency *Dependency) ParseTarget(Target []string) {
	dependency.Target = Target
	if len(Target[0]) == 0 {
		logrus.Fatal(`unexpected empty value for target`)
	}

	Target[0] = os.Expand(Target[0], dependency.expandEnvMapper)

	if Target[0][0:1] == "/" && Target[0][len(Target[0])-1:] == "/" {
		dependency.Type = "scripts-dir"
	} else if Target[0][0:1] == "/" {
		dependency.Type = "script"
	} else if tools.ValidateUrl(Target[0]) {
		dependency.Type = "http"
	} else if strings.Contains(Target[0], ":") {
		parts := strings.Split(Target[0], ":")
		if len(parts) > 3 {
			logrus.Fatal(`unexpected value for target: "%v"`, Target[0])
		}
		if len(parts) == 3 {
			switch parts[0] {
			case "tcp":
				dependency.Type = "tcp"
			default:
				logrus.Fatalf(`unexpected value for target: "%v"`, Target[0])
			}
		} else {
			dependency.Type = "tcp"
		}
	} else {
		dependency.Type = "ping"
	}

	dependency.expandEnvInTarget()

	if ok, err := dependency.validateDependencyTarget(); !ok {
		logrus.Fatal(err)
	}
}

func (dependency *Dependency) ParseMap(depMap map[string]interface{}) {
	dependency.setDependencyType(depMap["type"].(string))

	switch depMap["retry"].(type) {
	case string:
		retry, err := dependency.parseRetry(depMap["retry"].(string))
		if err != nil {
			logrus.Fatal(err)
		}
		dependency.Retry = retry
	case float64:
		dependency.Retry = int(depMap["retry"].(float64))
	case bool:
		dependency.Retry = depMap["retry"].(bool)
	}

	dependency.parseTimeout(depMap["timeout"])
	dependency.parseDelay(depMap["delay"])

	Target, err := decode.ToStrings(depMap["target"])
	if err != nil {
		logrus.Fatalf(`invalid dependency target, expected string or array of strings, type:"%T",value:"%v"`, depMap["target"], depMap["target"])
	}
	dependency.Target = Target

	dependency.expandEnvInTarget()

	if ok, err := dependency.validateDependencyTarget(); !ok {
		logrus.Fatal(err)
	}

	if cleanEnv, ok := depMap["cleanEnv"]; ok {
		switch cleanEnv.(type) {
		case bool:
			cleanEnvB := cleanEnv.(bool)
			dependency.CleanEnv = &cleanEnvB
		default:
			logrus.Fatalf(`invalid dependency cleanEnv, expected bool, type:"%T",value:"%v"`, cleanEnv, cleanEnv)
		}
	}

	if env, ok := depMap["env"]; ok {
		switch env.(type) {
		case map[string]string:
			dependency.Env = env.(map[string]string)
		case map[string]interface{}:
			decode.ToStruct(env, &dependency.Env)
		default:
			logrus.Fatalf(`invalid dependency env, expected map[string]string, type:"%T",value:"%v"`, env, env)
		}
	}

}

func (dependency *Dependency) expandEnvInTarget() {
	for i, val := range dependency.Target {
		dependency.Target[i] = os.Expand(val, dependency.expandEnvMapper)
	}
}
func (dependency *Dependency) expandEnvMapper(key string) string {
	if val, ok := dependency.EnvironMap[key]; ok {
		return val
	}
	return ""
}

func (dependency *Dependency) parseRetry(retryString string) (interface{}, error) {
	var err error
	var retry interface{}
	if retryString == "" {
		retry = nil
	} else if retryString == "1" {
		retry = 1
	} else {
		b, err := strconv.ParseBool(retryString)
		if err == nil {
			retry = b
		} else {
			i, err := strconv.Atoi(retryString)
			if err != nil {
				err = fmt.Errorf(`Unexpected retry format "%v", expected int (e.g: 10) or bool (e.g "true")`, retryString)
			}
			retry = i
		}
	}
	return retry, err
}

func (dependency *Dependency) parseTimeout(retryTimeout interface{}) {
	if retryTimeout == nil {
		return
	}
	retryTimeoutDuration, err := decode.Duration(retryTimeout)
	if err != nil {
		err2 := fmt.Sprintf(`Unexpected retry-timeout format "%v", expected seconds (e.g: 60) or duration (e.g 1h15m30s)`, retryTimeout)
		logrus.Fatal(errors.Wrap(err, err2))
	}
	dependency.Timeout = &retryTimeoutDuration
}

func (dependency *Dependency) parseDelay(retryDelay interface{}) {
	if retryDelay == nil {
		return
	}
	retryDelayDuration, err := decode.Duration(retryDelay)
	if err != nil {
		err2 := fmt.Sprintf(`Unexpected retry-timeout format "%v", expected seconds (e.g: 60) or duration (e.g 1h15m30s)`, retryDelay)
		logrus.Fatal(errors.Wrap(err, err2))
	}
	dependency.Delay = &retryDelayDuration
}

func (dependency *Dependency) setDependencyType(Type string) {
	switch Type {
	case
		"ping",
		"tcp",
		"script",
		"scripts-dir",
		"http":
		dependency.Type = Type
	default:
		logrus.Fatalf(`unexpected value "%v" of dependency type, expected "host","tcp", "script", "scripts-dir" or "http"`, Type)
	}
}

func (dependency *Dependency) validateDependencyTarget() (bool, error) {
	switch dependency.Type {
	case "ping":
		targetLength := len(dependency.Target)
		if targetLength == 1 {
			return true, nil
		} else {
			err := errors.New(`Unexpected number of host "` + strconv.Itoa(targetLength) + `", expected 1 host per dependency definition`)
			return false, err
		}
	case "tcp":
		netArgsNumberErrMsg := `Unexpected tcp target, expected [host, port], [host:port], host:port or tcp:host:port`
		targetLength := len(dependency.Target)
		if targetLength == 2 {
			return true, nil
		} else if targetLength == 1 {
			netArgs := strings.Split(dependency.Target[0], ":")
			lenNetArgs := len(netArgs)
			if lenNetArgs == 3 {
				netArgs = netArgs[1:3]
			} else if lenNetArgs != 2 {
				return false, errors.New(netArgsNumberErrMsg)
			}
			host := netArgs[0]
			if !(tools.ValidateHostname(host) || tools.ValidateIpAddress(host)) {
				return false, errors.New(`Unexpected tcp target, invalid host (dns name or ip): "` + host + `"`)
			}
			port := netArgs[1]
			if !tools.ValidatePort(port) {
				return false, errors.New(`Unexpected tcp target, invalid port: "` + port + `"`)
			}
			return true, nil
		} else {
			return false, errors.New(netArgsNumberErrMsg)
		}
	case "script":
		cmd := dependency.Target[0]
		_, lookErr := exec.LookPath(cmd)
		if lookErr != nil {
			return false, lookErr
		}
		return true, nil
	case "scripts-dir":
		targetLength := len(dependency.Target)

		if targetLength > 1 {
			return false, fmt.Errorf(`Unexpected scripts-dir target "%v", expected [dir]`, dependency.Target)
		}

		dir := dependency.Target[0]
		dirStat, err := os.Stat(dir)
		if !os.IsNotExist(err) {
			if err != nil {
				return false, err
			}
			if !dirStat.IsDir() {
				return false, fmt.Errorf(`Target dir %v is not a directory`, dir)
			}
		}
		return true, nil
	case "http":
		targetLength := len(dependency.Target)

		if targetLength < 1 {
			return false, fmt.Errorf(`Unexpected http target %v, expected [url], [url, httpCode] or [url, httpCode1RangeStart-httpCode1RangeEnd, !notHttpCode2, httpCode3]`, dependency.Target)
		}

		url := dependency.Target[0]
		if !tools.ValidateUrl(url) {
			return false, fmt.Errorf(`Unexpected http target, invalid url target: "%v"`, url)
		}

		codes := dependency.Target[1:]
		for _, code := range codes {

			if code[0:1] == "!" {
				code = code[1:]
			}

			var codeRange bool
			var codeStart string
			var codeEnd string
			codeSplit := strings.Split(code, "-")
			if len(codeSplit) == 2 {
				codeRange = true
				codeStart = codeSplit[0]
				codeEnd = codeSplit[1]
			}

			httpCodeErrMsg := `Unexpected http target, invalid http code target: "%v"`
			if codeRange {
				if isValidHttpCode := tools.ValidateHttpCode(codeStart); !isValidHttpCode {
					return false, fmt.Errorf(httpCodeErrMsg, codeStart)
				}
				if isValidHttpCode := tools.ValidateHttpCode(codeEnd); !isValidHttpCode {
					return false, fmt.Errorf(httpCodeErrMsg, codeEnd)
				}
			} else {
				if isValidHttpCode := tools.ValidateHttpCode(code); !isValidHttpCode {
					return false, fmt.Errorf(httpCodeErrMsg, code)
				}
			}

		}
		return true, nil

	}
	return false, nil
}

func NewDependency(dep interface{}, dependencyGroup *DependencyGroup) *Dependency {
	dependency := &Dependency{}

	dependency.DependencyGroup = dependencyGroup

	dependency.parseEnviron()

	switch dep.(type) {
	case string:
		dependency.ParseTarget(strings.Split(dep.(string), " "))
	default:
		target, err := decode.ToStrings(dep)
		if err != nil {
			logrus.Fatal(`invalid dependency, expected map, array or string, received type:"%T",value:"%v"`, dep, dep)
		}
		dependency.ParseTarget(target)
	case map[string]interface{}:
		depMap := dep.(map[string]interface{})
		dependency.ParseMap(depMap)
	}

	cfg := dependency.DependencyGroup.App.GetConfig()
	if dependency.Retry == nil {
		retry, err := dependency.parseRetry(cfg.Retry)
		if err != nil {
			logrus.Fatal(err)
		}
		dependency.Retry = retry
	}

	if dependency.Timeout == nil {
		dependency.Timeout = &cfg.Timeout
	}
	if dependency.Delay == nil {
		dependency.Delay = &cfg.Delay
	}

	return dependency
}

func (dependency *Dependency) parseEnviron() {
	cfg := dependency.DependencyGroup.App.GetConfig()
	if dependency.Env == nil {
		dependency.Env = cfg.Env
	}
	if dependency.CleanEnv == nil {
		dependency.CleanEnv = &cfg.CleanEnv
	}

	var environ []string
	if !(*dependency.CleanEnv) {
		environ = dependency.DependencyGroup.Environ
	}
	dependency.EnvironMap = tools.EnvExpandMap(environ, dependency.Env)
	dependency.Environ = tools.EnvToPairs(dependency.EnvironMap)
}
