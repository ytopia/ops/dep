package waitfor

import (
	"context"
	"net/http"
	"strconv"
	"strings"
)

func CheckHttp(Target []string, ctx context.Context) bool {

	url := Target[0]

	var codes []string
	targetLength := len(Target)
	if targetLength == 1 {
		codes = []string{"200-299"}
	} else {
		codes = Target[1:]
	}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return false
	}
	resp, err := http.DefaultClient.Do(req.WithContext(ctx))
	if err != nil {
		return false
	}

	ok := false
	i := 0
	for _, codeString := range codes {

		var not bool
		if codeString[0:1] == "!" {
			not = true
			codeString = codeString[1:]
		} else {
			not = false
		}

		var codeRange bool
		var codeStart int
		var codeEnd int
		var codeEq int
		codeSplit := strings.Split(codeString, "-")
		if len(codeSplit) == 2 {
			codeRange = true
			codeStart, _ = strconv.Atoi(codeSplit[0])
			codeEnd, _ = strconv.Atoi(codeSplit[1])
		} else {
			codeEq, _ = strconv.Atoi(codeString)
		}

		if codeRange {
			if resp.StatusCode >= codeStart && resp.StatusCode <= codeEnd {
				if not {
					ok = false
				} else {
					ok = true
				}
			} else if i == 0 {
				if not {
					ok = true
				} else {
					ok = false
				}
			}
		} else {
			if resp.StatusCode == codeEq {
				if not {
					ok = false
				} else {
					ok = true
				}
			} else if i == 0 {
				if not {
					ok = true
				} else {
					ok = false
				}
			}
		}

		i++
	}

	return ok

}
