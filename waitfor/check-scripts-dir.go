package waitfor

import (
	"context"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"syscall"

	"github.com/sirupsen/logrus"
	"gitlab.com/ytopia/ops/dep/tools"
)

func CheckScriptsDir(scriptsDir string, env []string, ctx context.Context) bool {

	if _, err := os.Stat(scriptsDir); os.IsNotExist(err) {
		logrus.Debugf(`scripts-dir path "%v" doesn't exists, skipping`, scriptsDir)
		return true
	}

	var scripts []string

	err := filepath.Walk(scriptsDir, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			scripts = append(scripts, path)
		}
		return nil
	})
	if err != nil {
		logrus.Fatal(err)
	}
	sort.Strings(scripts)

	hookFunc := func(cmd *exec.Cmd) error {
		cmd.Env = append(cmd.Env, env...)
		go func(cmd *exec.Cmd) {
			select {
			case <-ctx.Done():
				if cmd.Process == nil {
					return
				}
				if pgid, err := syscall.Getpgid(cmd.Process.Pid); err == nil {
					syscall.Kill(-pgid, syscall.SIGKILL)
				}
			}
		}(cmd)
		return nil
	}

	for _, script := range scripts {

		contextFields := logrus.Fields{
			"script": script,
		}
		if err := tools.RunCmd([]string{script}, contextFields, hookFunc, ctx); err != nil {
			return false
		}

	}

	return true
}
