package main

import (
	"gitlab.com/ytopia/ops/dep/app"
)

func main() {
	app.New()
}
